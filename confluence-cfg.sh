# Set variables
CONFIGLOC=/var/atlassian/application-data/confluence
CONFIGFILE=${CONFIGLOC}/confluence.cfg.xml
SVRCONFIGLOC=/opt/atlassian/confluence/conf
SERVERFILE=${SVRCONFIGLOC}/server.xml
DBHOST=${dbhost}
DBNAME=${dbname}
DBUSER=${dbuser}
DBPASS=${dbpass}
SID=${sid}
LIC=${lic}

# Replace config parameters if supplied, but if not all are provided...
# delete the config file so as to start the new installation wizard
if [[ -n "${DBHOST}" && -n "${DBNAME}" && -n "${DBUSER}" && -n "${DBPASS}" && -n "${SID}" && -n "${LIC}" ]] ; then
    sed -i "/hibernate\.connection\.url/s/dbhost/${DBHOST}/g" ${CONFIGFILE}
    sed -i "/hibernate\.connection\.url/s/dbname/${DBNAME}/g" ${CONFIGFILE}
    sed -i "/hibernate\.connection\.username/s/dbuser/${DBUSER}/g" ${CONFIGFILE}
    sed -i "/hibernate\.connection\.password/s/dbpass/${DBPASS}/g" ${CONFIGFILE}
    sed -i "/server\.id/s/srvid/${SID}/g" ${CONFIGFILE}
    sed -i "/license\.message/s/srvlic/${LIC}/g" ${CONFIGFILE}
else
    rm -f ${CONFIGFILE}
fi

if [[ -n "${ssl_term_domain}" ]] ; then
    # Build proxy info
    PROXYNAME=${ssl_term_domain}
    PROXYPORT="443"
    SCHEME="https"

    var_name="PROXY"
    var_value="proxyName=\"${PROXYNAME}\" proxyPort=\"${PROXYPORT}\" scheme=\"${SCHEME}\""
    declare "$var_name=$var_value"

    # Insert proxy info
    awk -v "var=${PROXY}" '/coyote/ && !x {print var; x=1} 1' ${SERVERFILE} > /tmp/tmp.xml && cp /tmp/tmp.xml ${SERVERFILE}
fi

# Mount the persisted directories
if [[ -n "${nfs_mount_attachments_cmd}" && -n "${nfs_mount_thumbnails_cmd}" && -n "${nfs_mount_index_cmd}" ]] ; then
    eval ${nfs_mount_attachments_cmd}
    eval ${nfs_mount_thumbnails_cmd}
    eval ${nfs_mount_index_cmd}
fi
