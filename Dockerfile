################
# Confluence image
################

# Set the base image
FROM centos:7.2.1511

# File Author / Maintainer
MAINTAINER orion-pax dkhaws77@gmail.com

# Build Args
ARG CONFL_VERSION
ENV CONFL_VERSION $CONFL_VERSION

## INFO
# confluence.home = /var/atlassian/application-data/confluence
# confluence.install = /opt/atlassian/confluence/confluence/WEB-INF/
##

# Add the necessary files
COPY response.varfile /response.varfile
RUN chmod 644 /response.varfile

# Install Confluence
RUN \
curl -L https://api.bitbucket.org/2.0/repositories/%7Bec20011b-2903-4c97-bfa3-5ad0ad278dc9%7D/%7Bacfbedf0-beff-42a9-af2e-d63dfd94e10e%7D/downloads/atlassian-confluence-${CONFL_VERSION}-x64.bin > /atlassian-confluence-${CONFL_VERSION}-x64.bin && \
chmod 744 /atlassian-confluence-${CONFL_VERSION}-x64.bin && \
/atlassian-confluence-${CONFL_VERSION}-x64.bin -q -varfile /response.varfile && \
rm -rf /atlassian-confluence-${CONFL_VERSION}-x64.bin

####
## Post installation tasks
####

# Add dynamic data directories
RUN \
mkdir /var/atlassian/application-data/confluence/attachments && \
chown confluence:confluence /var/atlassian/application-data/confluence/attachments && \
chmod 755 /var/atlassian/application-data/confluence/attachments

RUN \
mkdir /var/atlassian/application-data/confluence/thumbnails && \
chown confluence:confluence /var/atlassian/application-data/confluence/thumbnails && \
chmod 755 /var/atlassian/application-data/confluence/thumbnails

RUN \
mkdir /var/atlassian/application-data/confluence/index && \
chown confluence:confluence /var/atlassian/application-data/confluence/index && \
chmod 755 /var/atlassian/application-data/confluence/index

# Put the MySQL driver in the Confluence installation
COPY mysql-connector-java-5.1.36-bin.jar /opt/atlassian/confluence/confluence/WEB-INF/lib/mysql-connector-java-5.1.36-bin.jar 
RUN chmod 644 /opt/atlassian/confluence/confluence/WEB-INF/lib/mysql-connector-java-5.1.36-bin.jar 

# Start and stop Confluence to ensure all structures are created
COPY confluence-init.sh /confluence-init.sh
RUN \
chmod 755 /confluence-init.sh
RUN \
/confluence-init.sh

# Add the config file; db conn, license, etc
COPY confluence.cfg.xml /var/atlassian/application-data/confluence/
RUN \
chown confluence:confluence /var/atlassian/application-data/confluence/confluence.cfg.xml && \
chmod 644 /var/atlassian/application-data/confluence/confluence.cfg.xml

# Add the setenv.sh for startup parameters
COPY setenv.sh /opt/atlassian/confluence/bin/
RUN \
chown root:root /opt/atlassian/confluence/bin/setenv.sh && \
chmod 755 /opt/atlassian/confluence/bin/setenv.sh

# Change owner and permissions to avoid severe error in startup.
RUN \
chown root:confluence /opt/atlassian/confluence/conf && \
chmod 775 /opt/atlassian/confluence/conf

# Add necessary utilities
RUN yum install -y nfs-utils && \
yum clean all

# Add the config and start scripts
COPY docker-cmd.sh /docker-cmd.sh
RUN chmod 744 /docker-cmd.sh

COPY confluence-cfg.sh /confluence-cfg.sh
RUN chmod 744 /confluence-cfg.sh

# Expose the Confluence port
EXPOSE 8090

# Run this on container startup
CMD ["/docker-cmd.sh"]
