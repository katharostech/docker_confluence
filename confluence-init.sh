#!/bin/bash
logfile=/var/atlassian/application-data/confluence/logs/atlassian-confluence.log

echo ""
echo "Waiting for Confluence to start..."
echo ""

if [ -f $logfile ]; then
    echo "Confluence has already been started before. No need to initialize."
    echo ""
else
    # Start Confluence
    su -c "/opt/atlassian/confluence/bin/start-confluence.sh" confluence

    # Wait for the logfile to show up
    while [ ! -f $logfile ];
    do
        sleep 1
    done

    # Give a few seconds to finish startup
    sleep 7

    echo "Confluence has been initialized...shutting down"
    echo ""

    # Stop Confluence
    su -c "/opt/atlassian/confluence/bin/stop-confluence.sh" confluence && \
    echo "Confluence has stopped"
    echo ""
fi
